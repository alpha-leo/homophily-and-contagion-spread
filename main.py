#!/usr/bin/env python

from classes.analysis import Analysis


model = Analysis('erdos_renyi_graph_10000_49590.gpickle', 'erdos_renyi', data_path='./data', results_path='./results')
model.network_properties()
model.threshold_distributions()
model.state_distributions(low=6, p_p=0.2, t_s=(1,))
model.state_distributions(low=6, p_p=0.2, t_s=(1, 2, 4, 6, 8, 10))
model.contagion_speed_distributions(low=6, p_p=0.2, size_fraction_s=(0.2, 0.4, 0.6, 0.8))
model.cascade_likelihood_distributions(p_p=0.2, low_s=(6, 8, 10, 12))

model = Analysis('barabasi_albert_graph_10000_5.gpickle', 'barabasi_albert', data_path='./data', results_path='./results')
model.network_properties()
model.threshold_distributions()
model.state_distributions(low=47, p_p=0.2, t_s=(1,))
model.state_distributions(low=47, p_p=0.2, t_s=(1, 10, 20, 30, 40, 50, 60))
model.contagion_speed_distributions(low=47, p_p=0.2, size_fraction_s=(0.2, 0.4, 0.6, 0.8))
model.cascade_likelihood_distributions(p_p=0.2, low_s=(42, 47, 52, 57, 62, 67))
