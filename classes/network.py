#!/usr/bin/env python

import itertools
import numpy as np
import networkx as nx


class Network:
    """ the class that is the base for our network """
    def __init__(self, graph, homophily, low, p_p):
        self.graph = graph  # the graph object
        self.homophily = homophily  # the homophily probability of the network
        self.low = low  # value for the low threshold
        self.p_p = p_p
        self.size = nx.number_of_nodes(self.graph)  # size of the network (number of agents)
        self.edges = nx.number_of_edges(self.graph)  # size of the network (number of agents)
        self.high = self.find_high()  # value for the high threshold
        self._number_of_low_agents = None
        self._number_of_same_low_edges = None
        self._number_of_different_edges = None
        self._number_of_same_high_edges = None
        self._number_of_revolted_agents = 0
        self.time = None

        # data about state and threshold of agents (in the form of hash table)
        # state 0 -> 'stay at home'
        # state 1 -> 'revolt'
        # threshold 0 -> low
        # threshold 1 -> high
        self.agents = np.recarray((self.size,), dtype=[('state', bool), ('threshold', bool)])
        self.agents['state'] = 0  # initial state of all agents
        self.assign_thresholds()

    def find_high(self):
        """ find the value for the high threshold """
        degrees = np.array([m for n, m in self.graph.degree()])  # extract degrees

        return np.max(degrees) + 2

    def set_threshold(self, parent_state):
        """
            set the threshold of the child according to the
            threshold of the parent with the homophily probability
        """
        if np.random.uniform(0, 1) < self.homophily:
            return parent_state
        else:
            return not parent_state

    def assign_thresholds(self):
        """ use BFS to assign thresholds high/low based on homophily """
        # print('[Info]:Network: assigning thresholds')
        self._number_of_low_agents = 0  # reset number of low agents
        self._number_of_same_high_edges = 0  # reset number of same low edges
        self._number_of_same_low_edges = 0  # reset number of same low edges
        self._number_of_different_edges = 0  # reset number of different edges
        self.time = 0  # reset time

        init_agent = np.random.randint(self.size)  # find the first node randomly
        visited = np.zeros(self.size, dtype=bool)  # make visited list for bfs
        self.bfs_assignment(visited, init_agent)

        for node in range(self.size):
            if visited[node] == 0:
                self.bfs_assignment(visited, node)

    def bfs_assignment(self, visited, init_agent):
        self.agents['threshold'][init_agent] = 0
        visited[init_agent] = True
        self._number_of_low_agents += 1
        parents = [init_agent]
        children = []

        while True:
            while parents:
                parent = parents.pop(0)
                parent_state = self.agents['threshold'][parent]

                for node in self.graph.neighbors(parent):
                    if visited[node] == 0:  # if new node
                        visited[node] = True  # set as visited
                        self.agents['threshold'][node] = self.set_threshold(parent_state)  # assign threshold based
                        # on parent and homophily
                        if self.agents['threshold'][node] == 0:
                            self._number_of_low_agents += 1

                        children.append(node)  # add node to children

                    if parent_state != self.agents['threshold'][node]:
                        self._number_of_different_edges += 1
                    elif parent_state == 0:
                        self._number_of_same_low_edges += 1
                    else:
                        self._number_of_same_high_edges += 1

            if len(children) == 0:  # all nodes are visited
                break
            else:
                parents, children = children, []  # old children become new parents

    def make_decisions(self):
        new_revolted_agents = []
        participated_p = np.random.rand(self.size)

        for parent_node in range(self.size):
            CK_nodes = np.fromiter(itertools.chain(self.graph.neighbors(parent_node), [parent_node]), int)
            participated_CK_nodes = CK_nodes[participated_p[CK_nodes] < self.p_p]
            is_parent_revolted = self.agents['state'][parent_node]

            d1_stayed = participated_CK_nodes[self.agents['state'][participated_CK_nodes] == 0]
            d1_stayed_low = d1_stayed[self.agents['threshold'][d1_stayed] == 0]
            if len(d1_stayed_low) >= self.low:
                new_revolted_agents.extend(d1_stayed_low)
                if parent_node in d1_stayed_low:
                    is_parent_revolted = True

            if not is_parent_revolted and participated_p[parent_node] < self.p_p:
                distance_2_neighbors = []
                for neighbor in CK_nodes:
                    distance_2_neighbors = itertools.chain(distance_2_neighbors, nx.neighbors(self.graph, neighbor))
                distance_2_neighbors = np.fromiter(distance_2_neighbors, dtype=int)
                distance_2_neighbors = np.unique(distance_2_neighbors)
                d2_participated_neighbors = distance_2_neighbors[participated_p[distance_2_neighbors] < self.p_p]
                d2_revolted = d2_participated_neighbors[self.agents['state'][d2_participated_neighbors] == 1]

                if len(d2_revolted) > 0:
                    if self.agents['threshold'][parent_node] == 0:
                        new_revolted_agents.append(parent_node)
                    elif (len(d2_revolted) >= self.high - 1) or (1 in self.agents['threshold'][d2_revolted]):
                        new_revolted_agents.append(parent_node)

        unique_new_revolted_agents = np.unique(new_revolted_agents)
        if len(unique_new_revolted_agents) > 0:
            self.agents['state'][unique_new_revolted_agents] = True
        self._number_of_revolted_agents += len(unique_new_revolted_agents)
        self.time += 1

    def render(self, time):
        for step in range(time):
            self.make_decisions()

    def number_of_low_agents(self):
        return self._number_of_low_agents

    def number_of_high_agents(self):
        return self.size - self._number_of_low_agents

    def number_of_same_low_edges(self):
        return self._number_of_same_low_edges / 2

    def number_of_same_high_edges(self):
        return self.edges - (self._number_of_same_low_edges + self._number_of_different_edges) / 2

    def number_of_same_edges(self):
        return self.edges - self._number_of_different_edges / 2

    def number_of_different_edge(self):
        return self._number_of_different_edges / 2

    def number_of_revolted_agents(self):
        return self._number_of_revolted_agents

    def number_of_stayed_agents(self):
        return self.size - self._number_of_revolted_agents

    def reset_time(self):
        self.time = 0
        self.agents['state'] = 0
        self._number_of_revolted_agents = 0
